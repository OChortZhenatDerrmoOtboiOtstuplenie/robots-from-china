import cv2 as cv
import numpy as np
import os
import csv

IMG_DIR = './pictures' # path to images folder | папка со всеми тестовыми картинками
OUTPUT_FILE = 'result_data.csv'

def detect1(img):
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # Filter and prepare the image
    img = cv.GaussianBlur(img, (0, 0), sigmaX = 1.5, sigmaY = 1.5) # Blur
    img = cv.Canny(img, 50, 150, 3) # Egdes search
    img = cv.GaussianBlur(img, (0, 0), sigmaX = 1.2, sigmaY = 1.2) # Extra blur

    circles = cv.HoughCircles(img, cv.HOUGH_GRADIENT, 1.05, 6000, param1 = 100, param2 = 70, minRadius = 0, maxRadius = 0) # Circles search

    if np.any(circles != None):
        circles = np.uint16(np.around(circles))
    else:
        circles = np.array([[[0,0,0]]])

    choose = circles[0,:]

    r, x, y = choose[0, 2], choose[0, 0], choose[0, 1]
    return r, x, y

# Output
if __name__ == '__main__':
    with open(OUTPUT_FILE, 'w', newline='', encoding='utf-8-sig') as resultfile:
        header = ['num', 'ball_r', 'ball_x', 'ball_y']
        csv_write = csv.writer(resultfile)
        csv_write.writerow(header)

        readfiles = sorted(os.listdir(IMG_DIR))
        num = 1
        for readfile in readfiles:
            img_path = os.path.join(IMG_DIR, readfile)
            img = cv.imread(img_path)
            r, x, y = detect1(img)
            data_row = [num, r, x, y]
            csv_write.writerow(data_row)
            num += 1
